describe('Flapper News App', function () {
    describe('Home', function () {
        var settings = {
            url: '/'
        };

        beforeEach(function () {
            browser().navigateTo(settings.url);
        });

        it('should h1 be flapper news', function () {
            expect(element('H1').html()).toEqual('Flapper News');
        });

        it('should load the index page', function () {
            browser().navigateTo('/#/');
            expect(browser().location().path()).toBe('/home');
        });
       //  it('should show login when clicking sign in', function() { 2 browser().navigateTo('/#/'); 3 element("a#login", "Sign in button").click(); 4 expect(browser().location().path()) 5 .toBe('/login'); 6 });

        
    });
});