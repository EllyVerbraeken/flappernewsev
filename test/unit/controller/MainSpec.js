describe('MainControllerTest', function () {
    describe('MainCtrl', function () {

        beforeEach(function () {
            module('flapperNews');
        });

        beforeEach(inject(function ($controller, $rootScope) {
            $scope = $rootScope.$new();
        }));
        it('should show the user as logged out', inject(function ($controller) {
            var scope = {},
            ctrl = $controller('MainCtrl', { $scope: scope });
            $scope.isLoggedIn = false;
            expect($scope.isLoggedIn).toBe(false);
        }));
        it('should load all posts', inject(function ($controller) {
            var scope = {},
            ctrl = $controller('MainCtrl', { $scope: scope });
            $scope.posts = [];
            expect($scope.posts.length).toBe(0);
        }));

    });

});