//Because we are adding an external module, we need to be sure to include it as a dependency in our app:
var app = angular.module('flapperNews', ['ui.router']);

//Now that we have ui-router included, we need to configure it. 
//In our app.js, we're going to use the Angular config() function to setup a home state.
//Create the config block for our app and configure a home state using $stateProvider and $urlRouterProvider. 
//Use otherwise() to redirect unspecified routes.
app.config([
'$stateProvider',
'$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('home', {
  url: '/home',
  templateUrl: '/home.html',
  controller: 'MainCtrl',
  resolve: {
    postPromise: ['posts', function(posts){
      return posts.getAll();
    }]
  }
})
  /*  .state('posts', {
  url: '/posts/{id}',
  templateUrl: '/posts.html',
  controller: 'PostsCtrl'
});*/
.state('posts', {
  url: '/posts/{id}',
  templateUrl: '/posts.html',
  controller: 'PostsCtrl',
  resolve: {
    post: ['$stateParams', 'posts', function($stateParams, posts) {
      return posts.get($stateParams.id);
    }]
  }
})
.state('login', {
  url: '/login',
  templateUrl: '/login.html',
  controller: 'AuthCtrl',
  onEnter: ['$state', 'auth', function($state, auth){
    if(auth.isLoggedIn()){
      $state.go('home');
    }
  }]
})
.state('register', {
  url: '/register',
  templateUrl: '/register.html',
  controller: 'AuthCtrl',
  onEnter: ['$state', 'auth', function($state, auth){
    if(auth.isLoggedIn()){
      $state.go('home');
    }
  }]
});

  $urlRouterProvider.otherwise('/home');
}]);
//Here we set up our home route. You'll notice that the state is given a name ('home'), 
//URL ('/home'), and template URL ('/home.html'). 
//We've also told Angular that our new state should be controlled by MainCtrl. 
//Finally, using the otherwise() method we've specified what should happen 
//if the app receives a URL that is not defined. All that's left to do is define the home.html template. 
//Instead of creating a new file, we are going to move most of our HTML into an inline template.


app.controller('MainCtrl', [
'$scope',

'posts',
'auth',
function ($scope,posts, auth) {
    $scope.isLoggedIn = auth.isLoggedIn;
    $scope.test = 'Hello world!';
    $scope.posts = posts.posts;

    $scope.addPost = function(){
    
    if(!$scope.title || $scope.title === '') { return; }
    $scope.posts.push({title: $scope.title, upvotes: 0});
    $scope.title = '';
    };

  /*  $scope.addPost = function(){
    if(!$scope.title || $scope.title === '') { return; }
    $scope.posts.push({
    title: $scope.title,
    link: $scope.link,
    upvotes: 0
    });
    $scope.title = '';
    $scope.link = '';
    };*/

    $scope.addPost = function(){
  if(!$scope.title || $scope.title === '') { return; }
  posts.create({
    title: $scope.title,
    link: $scope.link
  });
  $scope.title = '';
  $scope.link = '';
};

   /* $scope.incrementUpvotes = function(post) {
    post.upvotes += 1;
    };*/
    $scope.incrementUpvotes = function(post) {
  posts.upvote(post);
};

    $scope.posts.push({
  title: $scope.title,
  link: $scope.link,
  upvotes: 0,
  comments: [
    {author: 'Joe', body: 'Cool post!', upvotes: 0},
    {author: 'Bob', body: 'Great idea but everything is wrong!', upvotes: 0}
  ]
});

} ]);


app.controller('PostsCtrl', [
'$scope',
'posts',
'post',
'auth',
function($scope, posts, post, auth){

    $scope.isLoggedIn = auth.isLoggedIn;
  $scope.post = post;
/*
app.controller('PostsCtrl', [
'$scope',
'$stateParams',
'posts',
function($scope, $stateParams, posts){
    $scope.post = posts.posts[$stateParams.id];*/

  /*  $scope.addComment = function(){
  if($scope.body === '') { return; }
  $scope.post.comments.push({
    body: $scope.body,
    author: 'user',
    upvotes: 0
  });
  $scope.body = '';
};*/

$scope.addComment = function(){
  if($scope.body === '') { return; }
  posts.addComment(post._id, {
    body: $scope.body,
    author: 'user'
  }).success(function(comment) {
    $scope.post.comments.push(comment);
  });
  $scope.body = '';
};

$scope.incrementUpvotes = function(comment){
  posts.upvoteComment(post, comment);
};

}


]);

//
app.controller('AuthCtrl', [
'$scope',
'$state',
'auth',
function($scope, $state, auth){
  $scope.user = {};

  $scope.register = function(){
    auth.register($scope.user).error(function(error){
      $scope.error = error;
    }).then(function(){
      $state.go('home');
    });
  };

  $scope.logIn = function(){
    auth.logIn($scope.user).error(function(error){
      $scope.error = error;
    }).then(function(){
      $state.go('home');
    });
  };
}])
/////

.controller('NavCtrl', [
'$scope',
'auth',
function($scope, auth){
  $scope.isLoggedIn = auth.isLoggedIn;
  $scope.currentUser = auth.currentUser;
  $scope.logOut = auth.logOut;
}]);
//////////

app.factory('posts', ['$http', 'auth', function($http, auth){
//app.factory('posts', ['$http', function($http){
  var o = {
    posts: []
  };

   o.getAll = function() {
    return $http.get('/posts').success(function(data){
      angular.copy(data, o.posts);
    });
  };

  o.create = function(post) {
  return $http.post('/posts', post
  , {
    headers: {Authorization: 'Bearer '+auth.getToken()}
  }
  ).success(function(data){
    o.posts.push(data);
  });
};

//o.upvote = function(post) {
 // return $http.put('/posts/' + post._id + '/upvote')
o.upvote = function(post) {
  return $http.put('/posts/' + post._id + '/upvote', null, {
    headers: {Authorization: 'Bearer '+auth.getToken()}
  }).success(function(data){
    post.upvotes += 1;
  });
};
o.get = function(id) {
  return $http.get('/posts/' + id).then(function(res){
    return res.data;
  });
};

o.addComment = function(id, comment) {
  return $http.post('/posts/' + id + '/comments', comment, {
    headers: {Authorization: 'Bearer '+auth.getToken()}
  });

//o.addComment = function(id, comment) {
  //return $http.post('/posts/' + id + '/comments', comment);
};


o.upvoteComment = function(post, comment) {
  return $http.put('/posts/' + post._id + '/comments/'+ comment._id + '/upvote', null, {
    headers: {Authorization: 'Bearer '+auth.getToken()}
  })
//o.upvoteComment = function(post, comment) {
  //return $http.put('/posts/' + post._id + '/comments/'+ comment._id + '/upvote')
    .success(function(data){
      comment.upvotes += 1;
    });
};

  return o;

}])

app.factory('auth', ['$http', '$window', function($http, $window){
   var auth = {};

auth.saveToken = function (token){
  $window.localStorage['flapper-news-token'] = token;
};

auth.getToken = function (){
  return $window.localStorage['flapper-news-token'];
}

auth.isLoggedIn = function(){
  var token = auth.getToken();

  if(token){
    var payload = JSON.parse($window.atob(token.split('.')[1]));

    return payload.exp > Date.now() / 1000;
  } else {
    return false;
  }
};

auth.currentUser = function(){
  if(auth.isLoggedIn()){
    var token = auth.getToken();
    var payload = JSON.parse($window.atob(token.split('.')[1]));

    return payload.username;
  }
};

auth.register = function(user){
  return $http.post('/register', user).success(function(data){
    auth.saveToken(data.token);
  });
};

auth.logIn = function(user){
  return $http.post('/login', user).success(function(data){
    auth.saveToken(data.token);
  });
};

auth.logOut = function(){
  $window.localStorage.removeItem('flapper-news-token');
};

auth.logOut = function(){
  $window.localStorage.removeItem('flapper-news-token');
};
  return auth;
}])



 
app.directive('slider', function ($timeout) {
  return {
    restrict: 'AE',
	replace: true,
	scope:{
		posts: '='
	},
    link: function (scope, elem, attrs) {
	
		scope.currentIndex=0;

		scope.next=function(){
			scope.currentIndex<scope.posts.length-1?scope.currentIndex++:scope.currentIndex=0;
		};
		
		scope.prev=function(){
			scope.currentIndex>0?scope.currentIndex--:scope.currentIndex=scope.posts.length-1;
		};
		
		scope.$watch('currentIndex',function(){
			scope.posts.forEach(function(post){
				post.visible=false;
			});
			scope.posts[scope.currentIndex].visible=true;
		});
		
		/* Start: For Automatic slideshow*/
		
		var timer;
		
		var sliderFunc=function(){
			timer=$timeout(function(){
				scope.next();
				timer=$timeout(sliderFunc,5000);
			},5000);
		};
		
		sliderFunc();
		
		scope.$on('$destroy',function(){
			$timeout.cancel(timer);
		});
		
		/* End : For Automatic slideshow*/
		
    },
	template:'<div class="slider">'+
	'<div class="slide" ng-repeat="post in posts" ng-show="post.visible">'+
		            '<span ng-show="post.author">'+
           ' posted by <a>{{post.author}}</a> |'+
       ' </span><span class="glyphicon glyphicon-thumbs-up" ng-click="incrementUpvotes(post)"></span>'+
       ' {{post.upvotes}}'+
                   ' <span style="font-size:20px; margin-left:10px;">'+
                     '<a ng-show="post.link" href="{{post.link}}">{{post.title}}</a>'+
                        '<span ng-hide="post.link">'+
           ' {{post.title}}</span></span><span>'+

           ' <a href="#/posts/{{post._id}}">Comments</a></span></div>'+
	'<div class="arrows">'+
		'<a href="#" ng-click="prev()"><img src="/images/right-arrow.jpg"/></a> <a href="#" ng-click="next()" ><img src="/images/left-arrow.jpg"/></a>'+
	'</div></div>'
  }
});